package be.rlab.tehanu.domain

class TestMemory : Memory() {
    private val memory: MutableMap<String, String> = mutableMapOf()

    override fun retrieve(slotName: String): String? {
        return memory[slotName]
    }

    override fun store(slotName: String, jsonData: String) {
        memory[slotName] = jsonData
    }

    override fun clear() {
        memory.clear()
    }
}
