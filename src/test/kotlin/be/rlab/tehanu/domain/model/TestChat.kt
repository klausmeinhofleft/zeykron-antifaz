package be.rlab.tehanu.domain.model

import java.util.*
import kotlin.random.Random

data class TestChat(
    val id: UUID = UUID.randomUUID(),
    val telegramId: Long = Random(9999).nextLong(),
    val type: ChatType = ChatType.GROUP,
    val title: String? = "Test Chat Title",
    val description: String? = "Test Chat Description"
) {
    fun new(): Chat = Chat(
        id = id,
        telegramId = telegramId,
        type = type,
        title = title,
        description = description
    )
}
