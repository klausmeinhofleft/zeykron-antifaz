package me.ivmg.telegram.entities

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.mockito.Mockito

data class TestMessage(
    val chat: Chat = TestChat().new(),
    val from: User? = TestUser().new(),
    val text: String? = null,
    val entities: List<MessageEntity>? = emptyList()
) {
    fun new(): Message = mock(defaultAnswer = Mockito.RETURNS_DEEP_STUBS) {
        on { chat } doReturn chat
        on { from } doReturn from
        on { text } doReturn text
        on { entities } doReturn entities
    }
}
