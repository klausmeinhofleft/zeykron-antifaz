package me.ivmg.telegram.entities

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.mockito.Mockito.RETURNS_DEEP_STUBS

data class TestChat(
    val id: Long = 1964,
    val type: String = "group",
    val title: String? = "some title",
    val description: String? = "some description"
) {
    fun new(): Chat = mock(defaultAnswer = RETURNS_DEEP_STUBS) {
        on { id } doReturn id
        on { type } doReturn type
        on { title } doReturn title
        on { description } doReturn description
    }
}