package me.ivmg.telegram.entities

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.mockito.Mockito.RETURNS_DEEP_STUBS

data class TestUser(
    val id: Long = 4815,
    val firstName: String = "H.P.",
    val lastName: String? = "Lovecraft",
    val username: String? = "cthulhu"
) {
    fun new(): User = mock(defaultAnswer = RETURNS_DEEP_STUBS) {
        on { id } doReturn id
        on { firstName } doReturn firstName
        on { lastName } doReturn lastName
        on { username } doReturn username
    }
}