package be.rlab.antifaz.config

import be.rlab.antifaz.domain.CanHasKittenListener
import be.rlab.tehanu.domain.MessageListenerDefinition
import be.rlab.tehanu.domain.model.TextMessage
import org.springframework.context.support.beans

object ApplicationBeans {
    fun beans() = beans {
        // Listeners
        bean {
            MessageListenerDefinition(
                messageType = TextMessage::class,
                listener = CanHasKittenListener(
                    botConfig = ref()
                )
            )
        }
    }
}
