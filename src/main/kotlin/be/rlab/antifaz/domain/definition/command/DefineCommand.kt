package be.rlab.antifaz.domain.definition.command

import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.TextMessage
import fastily.jwiki.core.Wiki

class DefineCommand(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    private val wiki: Wiki
) : Command {

    companion object {
        private const val MAX_SUMMARY_LENGTH: Int = 500
    }

    override fun handle(
        context: MessageContext,
        message: TextMessage
    ): MessageContext {
        val term: String = message.text.substringAfter(name).trim().replace(" ", "_")

        return when {
            term.isNotEmpty() -> {
                wiki.getTextExtract(term)?.let { summary ->
                    val url = "https://${wiki.conf.hostname}/wiki/$term"
                    context.talk("${summary.take(MAX_SUMMARY_LENGTH)}[...] -> $url")
                } ?: let {
                    context.talk("No encontré nada con el criterio que pusiste")
                }
                context.done()
            }
            term.isEmpty() && context.retries == 0 -> {
                context.talk("Decime qué querés que busque")
                context.retry()
            }
            else -> {
                context.talk("Volvé a intentar y decime qué querés que busque: /explicar Lenin")
                context.done()
            }
        }
    }
}