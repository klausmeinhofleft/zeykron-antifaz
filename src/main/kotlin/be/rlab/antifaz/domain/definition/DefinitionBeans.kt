package be.rlab.antifaz.domain.definition

import be.rlab.antifaz.domain.definition.command.DefineCommand
import be.rlab.tehanu.domain.model.ChatType
import fastily.jwiki.core.Wiki
import org.springframework.context.support.beans

object DefinitionBeans {
    fun beans() = beans {
        bean {
            DefineCommand(
                name = "/explicar",
                scope = listOf(ChatType.PRIVATE, ChatType.GROUP),
                timeout = -1,
                wiki = Wiki("es.wikipedia.org")
            )
        }
    }
}
