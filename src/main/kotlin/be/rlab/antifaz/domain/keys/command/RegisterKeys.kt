package be.rlab.antifaz.domain.keys.command

import be.rlab.tehanu.domain.AccessControl
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.TextMessage
import be.rlab.tehanu.domain.model.User
import be.rlab.antifaz.domain.MemorySlots.KEYS
import be.rlab.antifaz.domain.keys.model.Key

/** Command to register new keys.
 */
class RegisterKeys(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    private val accessControl: AccessControl,
    memory: Memory
) : Command {

    data class KeyHolderInput(
        val id: String,
        val holder: String
    )

    private var keys: List<Key> by memory.slot(KEYS, listOf<Key>())

    override fun handle(
        context: MessageContext,
        message: TextMessage
    ): MessageContext {
        return context.parseInput(message.text, """
            Pasame la información de las nuevas llaves con el siguiente formato:

            id: un nombre para reconocer este juego de llaves
            holder: nombre de usuario de telegram que tiene las llaves
        """.trimIndent()) { keyHolderInput: KeyHolderInput ->
            val exists: Boolean = keys.any { key ->
                key.identifier == keyHolderInput.id
            }

            if (exists) {
                answer("ya existe una llave con el id ${keyHolderInput.id}")
            } else {
                val holder: User = require(
                    accessControl.findUserByName(keyHolderInput.holder.substringAfter("@")),
                    "no conozco al usuario ${keyHolderInput.holder}, decile que me hable o " +
                    "fijate si está bien escrito"
                )

                val newKey: Key = Key.new(
                    identifier = keyHolderInput.id,
                    holder = holder.id
                )

                keys = keys + newKey
                answer("listo, la nueva llave la tiene ${keyHolderInput.holder}")
            }
        }
    }
}
