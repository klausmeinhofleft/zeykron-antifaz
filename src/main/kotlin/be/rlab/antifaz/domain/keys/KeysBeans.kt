package be.rlab.antifaz.domain.keys

import be.rlab.antifaz.domain.keys.command.KeysHistory
import be.rlab.antifaz.domain.keys.command.KeysList
import be.rlab.antifaz.domain.keys.command.RegisterKeys
import be.rlab.antifaz.domain.keys.command.TransferKeys
import be.rlab.tehanu.domain.model.ChatType
import org.springframework.context.support.beans

object KeysBeans {
    fun beans() = beans {
        bean {
            KeysHistory(
                name = "/keys_history",
                scope = listOf(ChatType.GROUP, ChatType.PRIVATE),
                timeout = 60000,
                memory = ref()
            )
        }
        bean {
            KeysList(
                name = "/keys",
                scope = listOf(ChatType.GROUP, ChatType.PRIVATE),
                timeout = -1,
                accessControl = ref(),
                memory = ref()
            )
        }
        bean {
            RegisterKeys(
                name = "/keys_add",
                scope = listOf(ChatType.GROUP),
                timeout = 60000,
                accessControl = ref(),
                memory = ref()
            )
        }
        bean {
            TransferKeys(
                name = "/keys_transfer",
                scope = listOf(ChatType.GROUP),
                timeout = 60000,
                accessControl = ref(),
                memory = ref()
            )
        }
    }
}
