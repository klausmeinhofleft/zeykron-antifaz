package be.rlab.antifaz.domain.keys.command

import be.rlab.antifaz.domain.MemorySlots.KEYS
import be.rlab.antifaz.domain.keys.model.Key
import be.rlab.tehanu.domain.AccessControl
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.TextMessage
import be.rlab.tehanu.domain.model.User

/** Command to display the list of keys.
 */
class KeysList(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    private val accessControl: AccessControl,
    memory: Memory
) : Command {

    private var keys: List<Key> by memory.slot(KEYS, listOf<Key>())

    override fun handle(
        context: MessageContext,
        message: TextMessage
    ): MessageContext {
        val keysList: String = keys.joinToString("\n") { key ->
            val user: User = context.require(accessControl.findUserById(key.holder), "El usuario no existe")
            "${key.identifier}: @${user.userName ?: key.holder}"
        }

        return context.talk(keysList)
    }
}
