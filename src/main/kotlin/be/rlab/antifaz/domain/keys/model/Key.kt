package be.rlab.antifaz.domain.keys.model

/** Represents a single key.
 *
 * Keys have identifiers so it is easier to know who has which key.
 * They also have a history to understand what happened with a key over the time.
 */
data class Key(
    /** A key identifier. */
    val identifier: String,
    /** Id of the user responsible of this key. */
    val holder: Long,
    /** Key history. */
    val history: List<Event>
) {
    companion object {
        fun new(
            identifier: String,
            holder: Long
        ): Key = Key(
            identifier = identifier,
            holder = holder,
            history = emptyList()
        )
    }

    /** Transfers this key to another user.
     * @param newHolderId New holder.
     * @param cause Event that caused this key to be transferred.
     */
    fun transfer(
        newHolderId: Long,
        cause: Event
    ): Key = copy(
        holder = newHolderId,
        history = history + cause
    )
}
