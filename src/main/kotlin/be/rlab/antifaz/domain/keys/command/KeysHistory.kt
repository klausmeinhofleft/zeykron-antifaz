package be.rlab.antifaz.domain.keys.command

import be.rlab.antifaz.domain.MemorySlots.KEYS
import be.rlab.antifaz.domain.keys.model.Key
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.TextMessage

/** Command to display the history of a key.
 */
class KeysHistory(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    memory: Memory
) : Command {

    private var keys: List<Key> by memory.slot(KEYS, listOf<Key>())

    override fun handle(
        context: MessageContext,
        message: TextMessage
    ): MessageContext {
        val keyId: String = message.text.substringAfter(name).trim()
        val key: Key = context.require(
            keys.find { key ->
                key.identifier == keyId
            },
            "la llave $keyId no está registrada, registrala con /keys_add"
        )

        val history: String = key.history.joinToString("\n") { event ->
            "[${event.date.toString("YYYY-MM-dd")}] [@${event.userName}]: ${event.description}"
        }

        return context.talk(history)
    }
}
