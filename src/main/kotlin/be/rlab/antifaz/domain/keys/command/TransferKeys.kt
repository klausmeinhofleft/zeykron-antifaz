package be.rlab.antifaz.domain.keys.command

import be.rlab.antifaz.domain.MemorySlots.KEYS
import be.rlab.antifaz.domain.keys.model.Event
import be.rlab.antifaz.domain.keys.model.Key
import be.rlab.tehanu.domain.AccessControl
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.TextMessage
import be.rlab.tehanu.domain.model.User

/** Command to transfer a key from a user to another user.
 */
class TransferKeys(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    private val accessControl: AccessControl,
    memory: Memory
) : Command {

    data class KeyHolderInput(
        val key: String,
        val holder: String,
        val cause: String?
    )

    private var keys: List<Key> by memory.slot(KEYS, listOf<Key>())

    override fun handle(
        context: MessageContext,
        message: TextMessage
    ): MessageContext {
        return context.parseInput(message.text, """
            Decime qué llaves querés transferir con el siguiente formato:

            key: nombre de la llave
            holder: nombre de usuario de telegram
            cause: por qué se está transfiriendo la llave (opcional)
        """.trimIndent()) { keyHolderInput: KeyHolderInput ->
            val newHolder: User = require(
                accessControl.findUserByName(keyHolderInput.holder.substringAfter("@")),
                "no conozco al usuario ${keyHolderInput.holder}, decile que me hable o " +
                "fijate si está bien escrito"
            )
            val key: Key = require(
                keys.find { key ->
                    key.identifier == keyHolderInput.key
                },
                "la llave ${keyHolderInput.key} no está registrada, registrala con /keys_add"
            )
            val transferredKey: Key = key.transfer(
                newHolderId = newHolder.id,
                cause = Event.new(
                    userName = user.userName ?: user.id.toString(),
                    description = keyHolderInput.cause ?: "Se transfirió la llave ${key.identifier} a $newHolder"
                )
            )

            keys = keys.map { existingKey ->
                if (existingKey.identifier == key.identifier) {
                    transferredKey
                } else {
                    existingKey
                }
            }

            answer("listo, ahora la llave la tiene @${keyHolderInput.holder}")
        }
    }
}
