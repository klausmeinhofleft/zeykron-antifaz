package be.rlab.antifaz.domain

object MemorySlots {
    const val EMAILS_SLOT: String = "emails"
    const val CALENDAR_SLOT: String = "calendar"
    const val MESSAGES_SLOT: String = "messages"
    const val INBOXES_SLOT: String = "inboxes"
    const val SUBSCRIPTIONS_SLOT: String = "subscriptions"
    const val CONTENT_SLOT: String = "content"
    const val NOTIFICATIONS: String = "notifications"
    const val CHANNELS_SUBSCRIPTIONS: String = "channels_subscriptions"
    const val PANIC_ALERTS: String = "panic_alerts"
    const val KEYS: String = "keys"
}
