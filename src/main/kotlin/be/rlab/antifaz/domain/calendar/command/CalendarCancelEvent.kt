package be.rlab.antifaz.domain.calendar.command

import be.rlab.antifaz.domain.MemorySlots.CALENDAR_SLOT
import be.rlab.antifaz.domain.calendar.model.Event
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.TextMessage
import java.util.*

class CalendarCancelEvent(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    memory: Memory
) : Command {

    private var eventsByChat: Map<UUID, MutableList<Event>> by memory.slot(CALENDAR_SLOT, mapOf<UUID, MutableList<Event>>())

    override fun handle(
        context: MessageContext,
        message: TextMessage
    ): MessageContext {
        val events: MutableList<Event> = eventsByChat[context.chat.id] ?: mutableListOf()

        return if (events.isEmpty()) {
            context.talk("No hay eventos en el calendario")
        } else {
            context.parseInput(message.text, """
                Decime qué número de evento querés cancelar. Podés ver los números usando /calendar
            """.trimIndent()) { eventIndex: Int ->
                if (eventIndex < 1 || eventIndex > events.size) {
                    context.talk("El evento no existe, podés ver los números de eventos usando /calendar")
                } else {
                    events.removeAt(eventIndex - 1)
                    eventsByChat = eventsByChat + (context.chat.id to events)

                    context.talk("Listo, cancelé el evento")
                }
            }
        }
    }
}
