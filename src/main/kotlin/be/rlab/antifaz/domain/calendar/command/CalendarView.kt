package be.rlab.antifaz.domain.calendar.command

import be.rlab.antifaz.domain.MemorySlots.CALENDAR_SLOT
import be.rlab.antifaz.domain.calendar.model.Event
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.TextMessage
import me.ivmg.telegram.entities.ParseMode
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import java.util.*

class CalendarView(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    memory: Memory
) : Command {

    private val eventsByChat: Map<UUID, List<Event>> by memory.slot(CALENDAR_SLOT, mapOf<UUID, List<Event>>())

    private val monthFormatter: DateTimeFormatter = DateTimeFormat
        .forPattern("MMMM YYYY")
        .withLocale(Locale("es", "AR"))
    private val dayFormatter: DateTimeFormatter = DateTimeFormat
        .forPattern("EEEE d")
        .withLocale(Locale("es", "AR"))
    private val hourFormatter: DateTimeFormatter = DateTimeFormat
        .forPattern("HH:mm")

    override fun handle(
        context: MessageContext,
        message: TextMessage
    ): MessageContext {
        val calendar = calendar(context.chat.id)
        val calendarString = StringBuilder()

        if (calendar.isEmpty()) {
            context.talk("No hay eventos en el calendario")
        }

        var index = 1

        calendar.forEach { (month, events) ->
            calendarString.append("*${month.toString(monthFormatter)}* \n\n")

            groupByDay(events).forEach { (day, events) ->
                calendarString.append("${day.toString(dayFormatter)}\n")

                events.forEach { event ->
                    val end = event.end?.let {
                        "-${event.end.toString(hourFormatter)}"
                    } ?: ""
                    val eventTime = "${event.start.toString(hourFormatter)}$end"

                    calendarString.append("*[$index] $eventTime* ")
                    calendarString.append("_${event.title}: ${event.description}_\n")

                    index += 1
                }
            }
        }

        return context.talk(calendarString.toString(), ParseMode.MARKDOWN)
    }

    private fun calendar(chatId: UUID): Map<DateTime, List<Event>> {
        return eventsByChat[chatId]?.groupBy { event ->
            event.start.withDayOfMonth(1).withTimeAtStartOfDay()
        } ?: emptyMap()
    }

    private fun groupByDay(events: List<Event>): Map<DateTime, List<Event>> {
        return events.filter { event ->
            event.start.isAfter(DateTime.now().minusDays(1))
        }.groupBy { event ->
            event.start.withTimeAtStartOfDay()
        }
    }
}