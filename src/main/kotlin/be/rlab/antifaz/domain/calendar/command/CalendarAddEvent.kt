package be.rlab.antifaz.domain.calendar.command

import be.rlab.antifaz.domain.MemorySlots.CALENDAR_SLOT
import be.rlab.antifaz.domain.calendar.model.Event
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.TextMessage
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import java.util.*

class CalendarAddEvent(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    memory: Memory
) : Command {

    private var eventsByChat: Map<UUID, List<Event>> by memory.slot(CALENDAR_SLOT, mapOf<UUID, List<Event>>())
    private val hourFormatter: DateTimeFormatter = DateTimeFormat
        .forPattern("HH:mm")

    data class AddEventInput(
        @JsonFormat(pattern = "YYYY-MM-dd") @JsonProperty("fecha")
        val date: DateTime,
        @JsonProperty("inicio") val start: String,
        @JsonProperty("fin") val end: String?,
        @JsonProperty("titulo") val title: String,
        @JsonProperty("descripcion") val description: String
    )

    override fun handle(
        context: MessageContext,
        message: TextMessage
    ): MessageContext {
        return context.parseInput(message.text, """
            Mandame los datos del evento en el siguiente formato:

            fecha: 2019-04-27
            inicio: 10:00
            fin: 13:00 (opcional)
            titulo: Apertura del FLISoL
            descripcion: primera charla del FLISoL
        """.trimIndent()) { addEventInput: AddEventInput ->
            val event = Event.new(
                start = addEventInput.date.withTime(
                    hourFormatter.parseDateTime(addEventInput.start).toLocalTime()
                ),
                end = addEventInput.end?.let {
                    addEventInput.date.withTime(
                        hourFormatter.parseDateTime(addEventInput.end).toLocalTime()
                    )
                },
                title = addEventInput.title,
                description = addEventInput.description
            )

            val events: List<Event> = eventsByChat[context.chat.id] ?: emptyList()

            eventsByChat = eventsByChat + (
                context.chat.id to (events + event).sortedBy { existingEvent ->
                    existingEvent.start
                }
            )

            context.talk("Listo, ya agregué el evento")
        }
    }
}
