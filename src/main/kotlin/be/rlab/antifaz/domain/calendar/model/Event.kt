package be.rlab.antifaz.domain.calendar.model

import org.joda.time.DateTime
import java.util.*

data class Event(
    val id: UUID,
    val start: DateTime,
    val end: DateTime?,
    val title: String,
    val description: String
) {

    companion object {
        fun new(
            start: DateTime,
            end: DateTime?,
            title: String,
            description: String
        ): Event = Event(
            id = UUID.randomUUID(),
            start = start,
            end = end,
            title = title,
            description = description
        )
    }

    fun update(
        start: DateTime,
        end: DateTime?,
        title: String,
        description: String
    ): Event = copy(
        start = start,
        end = end,
        title = title,
        description = description
    )
}
