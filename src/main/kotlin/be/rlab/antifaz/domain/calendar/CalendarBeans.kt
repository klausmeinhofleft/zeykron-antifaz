package be.rlab.antifaz.domain.calendar

import be.rlab.antifaz.domain.calendar.command.CalendarAddEvent
import be.rlab.antifaz.domain.calendar.command.CalendarCancelEvent
import be.rlab.antifaz.domain.calendar.command.CalendarView
import be.rlab.tehanu.domain.model.ChatType
import org.springframework.context.support.beans

object CalendarBeans {
    fun beans() = beans {
        // Commands
        bean {
            CalendarView(
                name = "/calendar",
                scope = listOf(ChatType.GROUP, ChatType.PRIVATE),
                timeout = -1,
                memory = ref()
            )
        }
        bean {
            CalendarAddEvent(
                name = "/calendar_add_event",
                scope = listOf(ChatType.GROUP, ChatType.PRIVATE),
                timeout = 60000,
                memory = ref()
            )
        }
        bean {
            CalendarCancelEvent(
                name = "/calendar_cancel_event",
                scope = listOf(ChatType.GROUP, ChatType.PRIVATE),
                timeout = 20000,
                memory = ref()
            )
        }
    }
}
