package be.rlab.antifaz.domain.panic.command

import be.rlab.antifaz.domain.MemorySlots.PANIC_ALERTS
import be.rlab.antifaz.domain.panic.model.PanicAlert
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.TextMessage

/** Command to enable the panic alert on a chat.
 *
 * This command adds the current chat to the user's panic alert allies, so
 * when a panic alert is triggered all messages are forwarded to those chats.
 */
class EnablePanicAlert(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    memory: Memory
) : Command {

    private var alerts: List<PanicAlert> by memory.slot(PANIC_ALERTS, listOf<PanicAlert>())

    override fun handle(
        context: MessageContext,
        message: TextMessage
    ): MessageContext {
        val index: Int = alerts.indexOfFirst { panicAlert ->
            panicAlert.userId == context.user.id
        }

        alerts = if (index > -1) {
            alerts.map { panicAlert ->
                if (panicAlert.userId == context.user.id) {
                    panicAlert.addAllyChat(context.chat.id)
                } else {
                    panicAlert
                }
            }
        } else {
            alerts + PanicAlert.new(context.user.id, context.chat.id)
        }

        context.answer(
            "listo, la alerta de pánico se activó en este chat, cuando me mandes " +
            "un mensaje privado con tu ubicación se va a activar la alerta y voy a " +
            "mandar todos los mensajes que me escribas a este chat."
        )

        return context
    }
}
