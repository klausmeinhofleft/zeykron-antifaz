package be.rlab.antifaz.domain.panic.model

import java.util.*

/** Configures a panic alert for a single user.
 *
 * The panic alert is enabled the first time a user shares the location
 * with the bot. Once enabled, all messages will be forwarded to all
 * ally chats.
 */
data class PanicAlert(
    /** Unique identifier for this configuration. */
    val alertId: UUID,
    /** User identifier of the user that created this alert. */
    val userId: Long,
    /** List of chats to send messages when the alert is enabled. */
    val allyChats: Set<UUID>,
    /** Indicates whether the alert is enabled. */
    val alert: Boolean
) {
    companion object {
        fun new(
            userId: Long,
            chatId: UUID
        ): PanicAlert = PanicAlert(
            alertId = UUID.randomUUID(),
            userId = userId,
            allyChats = setOf(chatId),
            alert = false
        )
    }

    fun addAllyChat(chatId: UUID): PanicAlert = copy(
        allyChats = allyChats + chatId
    )

    fun removeAllyChat(chatId: UUID): PanicAlert = copy(
        allyChats = allyChats.filter { allyChatId ->
            allyChatId != chatId
        }.toSet()
    )

    fun enable(): PanicAlert = copy(
        alert = true
    )

    fun disable(): PanicAlert = copy(
        alert = false
    )
}
