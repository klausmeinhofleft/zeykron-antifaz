package be.rlab.antifaz.domain.panic

import be.rlab.antifaz.domain.panic.command.DisablePanicAlert
import be.rlab.antifaz.domain.panic.command.EnablePanicAlert
import be.rlab.tehanu.domain.MessageListenerDefinition
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.Message
import org.springframework.context.support.beans

object PanicBeans {
    fun beans() = beans {
        bean {
            EnablePanicAlert(
                name = "/panic_alert_enable",
                scope = listOf(ChatType.GROUP),
                timeout = 20000,
                memory = ref()
            )
        }
        bean {
            DisablePanicAlert(
                name = "/panic_alert_disable",
                scope = listOf(ChatType.GROUP, ChatType.PRIVATE),
                timeout = 20000,
                memory = ref()
            )
        }
        bean {
            MessageListenerDefinition(
                messageType = Message::class,
                listener = ForwardMessages(
                    memory = ref()
                )
            )
        }
    }
}
