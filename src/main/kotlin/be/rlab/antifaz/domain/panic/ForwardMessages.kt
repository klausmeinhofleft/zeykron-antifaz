package be.rlab.antifaz.domain.panic

import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.MessageListener
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.LocationMessage
import be.rlab.tehanu.domain.model.Message
import be.rlab.tehanu.domain.model.TextMessage
import be.rlab.antifaz.domain.MemorySlots.PANIC_ALERTS
import be.rlab.antifaz.domain.panic.model.PanicAlert
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/** Triggers and forwards panic alert messages.
 *
 * It triggers the panic alert the first time a [LocationMessage] is sent in private
 * to the bot. Then it forwards all messages to the allies chats until the panic alert
 * is disabled by the user.
 */
class ForwardMessages(
    memory: Memory
) : MessageListener<Message> {

    private val logger: Logger = LoggerFactory.getLogger(ForwardMessages::class.java)

    private var alerts: List<PanicAlert> by memory.slot(PANIC_ALERTS, listOf<PanicAlert>())

    override fun applies(
        context: MessageContext,
        message: Message
    ): Boolean {
        val userHasAlert: Boolean = alerts.any { panicAlert ->
            panicAlert.userId == context.user.id
        }
        return userHasAlert && context.chat.type == ChatType.PRIVATE
    }

    override fun handle(
        context: MessageContext,
        message: Message
    ): MessageContext {
        if (message is LocationMessage) {
            logger.info("activating panic alert for $context")

            // Location messages enables the alert
            alerts = alerts.map { panicAlert ->
                if (panicAlert.userId == context.user.id) {
                    panicAlert.enable()
                } else {
                    panicAlert
                }
            }
        }

        val panicAlert: PanicAlert = alerts.find { panicAlert ->
            panicAlert.userId == context.user.id
        } ?: throw RuntimeException("Alert not found.")

        if (panicAlert.alert) {
            logger.info("forwarding panic alert message from $context to allies")

            panicAlert.allyChats.forEach { chatId ->
                when (message) {
                    is LocationMessage -> context.sendLocation(chatId, message.location)
                    is TextMessage -> context.talkTo(chatId, message.text)
                }
            }
        }

        return context
    }
}
