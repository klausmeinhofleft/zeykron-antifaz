package be.rlab.antifaz.domain.panic.command

import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.TextMessage
import be.rlab.antifaz.domain.MemorySlots.PANIC_ALERTS
import be.rlab.antifaz.domain.panic.model.PanicAlert

/** Command to disable panic alerts.
 *
 * It disables the panic alert for a chat if it's executed within a group,
 * or disables the panic alert once activated if it's executed in private.
 */
class DisablePanicAlert(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    memory: Memory
) : Command {

    private var alerts: List<PanicAlert> by memory.slot(PANIC_ALERTS, listOf<PanicAlert>())

    override fun applies(
        context: MessageContext,
        message: TextMessage
    ): Boolean {
        val panicAlertEnabled: Boolean = alerts.any { panicAlert ->
            panicAlert.userId == context.user.id &&
            panicAlert.allyChats.contains(context.chat.id)
        }

        if (!panicAlertEnabled && context.chat.type != ChatType.PRIVATE) {
            context.answer("la alerta de pánico no está activada para este chat")
        }

        return panicAlertEnabled || context.chat.type == ChatType.PRIVATE
    }

    override fun handle(
        context: MessageContext,
        message: TextMessage
    ): MessageContext {
        alerts = alerts.map { panicAlert ->
            if (panicAlert.userId == context.user.id) {
                if (context.chat.type == ChatType.GROUP) {
                    panicAlert.removeAllyChat(context.chat.id)
                } else {
                    panicAlert.disable()
                }
            } else {
                panicAlert
            }
        }

        context.answer("listo, la alerta de pánico se desactivó")

        return context
    }
}
