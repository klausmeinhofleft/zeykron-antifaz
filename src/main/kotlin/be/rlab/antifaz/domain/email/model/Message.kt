package be.rlab.antifaz.domain.email.model

import org.joda.time.DateTime
import javax.mail.Flags
import javax.mail.Message as IMAPMessage

data class Message(
    val subject: String,
    val from: String,
    val recipients: List<String>,
    val body: String,
    val sentDate: DateTime,
    val flags: List<Flag>,
    private val backingMessage: IMAPMessage
) {
    fun markAsRead() {
        backingMessage.setFlag(Flags.Flag.SEEN, true)
    }

    fun unseen(): Boolean =
        !backingMessage.isSet(Flags.Flag.SEEN)

    fun isFlagged(expectedFlags: List<Flag>): Boolean =
        expectedFlags.all { flag ->
            flags.contains(flag)
        } || (expectedFlags.contains(Flag.UNSEEN) && unseen())
}
