package be.rlab.antifaz.domain.email.model

import be.rlab.antifaz.domain.email.model.ConnectionInfo
import java.util.*

data class ChatConnectionInfo(
    val chatId: UUID,
    val host: String,
    val port: Int,
    val userName: String,
    val password: String,
    val connectionProperties: Map<String, String> = emptyMap()
) {
    fun asConnectionInfo(): ConnectionInfo = ConnectionInfo(
        host = host,
        port = port,
        userName = userName,
        password = password,
        connectionProperties = connectionProperties
    )

    override fun toString(): String = "$host/$userName"
}
