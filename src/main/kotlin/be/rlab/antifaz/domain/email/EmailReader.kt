package be.rlab.antifaz.domain.email

import be.rlab.antifaz.domain.email.model.ConnectionInfo
import be.rlab.antifaz.domain.email.model.Flag
import be.rlab.antifaz.domain.email.model.FolderSubscription
import be.rlab.antifaz.domain.email.model.Message
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.mail.Folder

class EmailReader(private val client: IMAPClient) {

    companion object Connection {
        private val logger: Logger = LoggerFactory.getLogger(EmailReader::class.java)

        fun connect(
            connectionInfo: ConnectionInfo,
            callback: (EmailReader.() -> Unit)? = null
        ): EmailReader {
            logger.info("Connecting to $connectionInfo")

            return EmailReader(IMAPClient(connectionInfo)).apply {
                logger.info("Connection established")
                callback?.invoke(this)
            }
        }
    }

    private val logger: Logger = LoggerFactory.getLogger("${javaClass.simpleName} - ${client.accountName}")

    private val subscriptions: MutableMap<String, FolderSubscription> = mutableMapOf()
    private var terminate: Boolean = false
    val accountDescription: String = client.accountDescription

    fun openFolder(
        folderName: String,
        callback: ((Folder) -> Unit)? = null
    ): Folder {
        logger.info("Opening folder $folderName")

        if (subscriptions.containsKey(folderName)) {
            logger.info("Folder already opened, using existing handle")
            return subscriptions[folderName]?.folder
                ?: throw RuntimeException("Folder not found")
        }

        val folder = client.open(folderName)
        val messageChannel: Channel<Message> = Channel()

        subscriptions[folderName] = FolderSubscription(
            folder = folder,
            channel = messageChannel
        )

        logger.info("Opened. Adding subscription to wait for messages on folder $folderName")
        client.subscribe(folder, messageChannel)
        callback?.invoke(folder)

        return folder
    }

    fun closeFolder(folder: Folder) {
        closeFolder(folder.name)
    }

    private fun closeFolder(folderName: String) {
        logger.info("Closing folder $folderName")

        subscriptions.remove(folderName)?.let { folderSubscription ->
            folderSubscription.channel.close()
            folderSubscription.folder.close(false)
        }
    }

    fun onMessage(
        folder: Folder,
        vararg flags: Flag,
        callback: (Message) -> Unit
    ) = GlobalScope.launch {

        val messageChannel: Channel<Message> = subscriptions[folder.name]?.channel
            ?: throw RuntimeException("Folder not found")

        logger.info("Listening for messages with flags ${flags.toList()} from folder ${folder.name}.")

        for (message in messageChannel) {
            if (message.isFlagged(flags.toList())) {
                logger.info("Got new message from folder ${folder.name}.")
                callback(message)
            } else {
                logger.info("Got new message from folder ${folder.name} " +
                        "but is hasn't required flags, skipping notification.")
            }
        }
    }

    fun listMessages(
        folder: Folder,
        vararg flags: Flag
    ): List<Message> {
        logger.info("Listing messages with flags: ${flags.toList()}")

        return flags.fold(emptyList()) { list, flag ->
            list + client.listMessages(folder, flag)
        }
    }

    fun shutdown() {
        val folderNames: List<String> = subscriptions.keys.toList()

        logger.info("Shutting down email reader")

        folderNames.forEach { folderName ->
            closeFolder(folderName)
        }
        terminate = true
        client.shutdown()
    }

    fun join() {
        logger.info("joining")

        while(!terminate) {
        }

        logger.info("terminated")
    }
}
