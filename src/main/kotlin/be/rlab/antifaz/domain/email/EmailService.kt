package be.rlab.antifaz.domain.email

import be.rlab.antifaz.domain.MemorySlots.EMAILS_SLOT
import be.rlab.antifaz.domain.email.model.ChatConnectionInfo
import be.rlab.antifaz.domain.email.model.Flag
import be.rlab.antifaz.domain.inbox.InboxService
import be.rlab.antifaz.domain.inbox.model.Content
import be.rlab.tehanu.domain.BotAware
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.Tehanu
import be.rlab.tehanu.domain.model.Chat
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*

class EmailService(
    memory: Memory,
    private val inboxService: InboxService
) : BotAware {

    private val logger: Logger = LoggerFactory.getLogger(EmailService::class.java)

    private val connections: List<ChatConnectionInfo> by memory.slot(EMAILS_SLOT, listOf<ChatConnectionInfo>())
    private val emailReaders: MutableMap<UUID, EmailReader> by lazy {
        connections.map { connectionInfo ->
            connectionInfo.chatId to createEmailReader(connectionInfo)
        }.toMap(mutableMapOf())
    }

    override lateinit var tehanu: Tehanu

    init {
        logger.info("Registering listener for slot '$EMAILS_SLOT' changes")
        memory.subscribe(EMAILS_SLOT, this::updateConnections)
    }

    fun findEmailReader(chat: Chat): EmailReader? {
        logger.debug("Finding email reader for chat $chat (exists: ${emailReaders.containsKey(chat.id)})")
        return emailReaders[chat.id]
    }

    private fun updateConnections() {
        logger.info("'$EMAILS_SLOT' changed, analyzing new email accounts")

        // Closes connection and reconnects to existing servers
        val evicted = emailReaders.filter { (chatId, _) ->
            connections.any { connectionInfo ->
                connectionInfo.chatId == chatId
            }
        }.map { (chatId, emailReader) ->
            logger.info("Account ${emailReader.accountDescription} removed from chat $chatId, shutting down email reader")
            emailReader.shutdown()
            chatId
        }
        evicted.forEach { chatId ->
            emailReaders.remove(chatId)
        }

        // Opens new connections
        connections.filter { connectionInfo ->
            !emailReaders.containsKey(connectionInfo.chatId)
        }.forEach { connectionInfo ->
            logger.info("Account $connectionInfo added for chat ${connectionInfo.chatId}")
            emailReaders[connectionInfo.chatId] = createEmailReader(connectionInfo)
        }
    }

    private fun createEmailReader(connectionInfo: ChatConnectionInfo): EmailReader {
        logger.info("Creating email reader for account $connectionInfo on chat ${connectionInfo.chatId}")

        return EmailReader.connect(connectionInfo.asConnectionInfo()) {
            openFolder("INBOX") { folder ->
                onMessage(folder, Flag.UNSEEN) { message ->
                    tehanu.sendMessage(connectionInfo.chatId, "Hay un nuevo mensaje: ${message.subject}")
                    inboxService.broadcast(
                        chatId = connectionInfo.chatId,
                        from = message.from,
                        subject = message.subject,
                        content = Content.text(message.body),
                        timestamp = message.sentDate
                    )
                }
            }
        }
    }
}