package be.rlab.antifaz.domain.email

import be.rlab.antifaz.domain.email.model.ConnectionInfo
import be.rlab.antifaz.domain.email.model.Flag
import be.rlab.antifaz.domain.email.model.Message
import com.sun.mail.imap.IMAPFolder
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.joda.time.DateTime
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue
import javax.mail.Folder
import javax.mail.Multipart
import javax.mail.Session
import javax.mail.Store
import javax.mail.event.MessageCountEvent
import javax.mail.event.MessageCountListener
import javax.mail.Message as IMAPMessage

class IMAPClient(
    private val connectionInfo: ConnectionInfo
) {

    companion object {
        // Sends a NOOP command every 5 minutes to avoid
        // server closing the connection.
        private const val NOOP_DELAY: Long = 1000 * 60 * 5
        private const val NOOP_COMMAND: String = "NOOP"
        // Waits for the queue to load in order to avoid race conditions.
        private const val QUEUE_LOAD_DELAY: Long = 1000
    }

    private val logger: Logger = LoggerFactory.getLogger("${javaClass.simpleName} - ${connectionInfo.userName}")

    private val store: Store
    val accountDescription: String = connectionInfo.toString()
    val accountName: String = connectionInfo.userName

    init {
        val properties = Properties().apply {
            putAll(connectionInfo.connectionProperties)
        }
        logger.debug("Creating email session with properties: {}", properties)
        val session = Session.getDefaultInstance(properties)

        logger.debug("Session created. Opening IMAP store")
        store = session.getStore("imaps").apply {
            logger.debug("Opened. Connecting to $connectionInfo")

            connect(
                connectionInfo.host, connectionInfo.port,
                connectionInfo.userName, connectionInfo.password
            )
        }
    }

    fun open(folderName: String): Folder {
        logger.debug("Retrieving folder $folderName from store")
        val folder: Folder = store.getFolder(folderName)
        require(folder.exists())

        logger.debug("Opening folder $folderName as read only")
        folder.open(Folder.READ_ONLY)
        noop(folder as IMAPFolder)

        return folder
    }

    fun listMessages(
        folder: Folder,
        flag: Flag
    ): List<Message> {
        // Fetch unseen messages from inbox folder
        logger.debug("Searching term ${flag.term} in folder ${folder.name}")

        val messages: Array<IMAPMessage> = folder.search(flag.term)

        logger.debug("Sorting messages")
        // Sort messages from recent to oldest
        messages.sortBy {
            it.sentDate
        }
        return messages.map(this::convertMessage)
    }

    fun subscribe(
        folder: Folder,
        messageChannel: Channel<Message>
    ) = GlobalScope.launch {

        val queue: BlockingQueue<Message> = LinkedBlockingQueue()

        require(folder is IMAPFolder)

        newMessageListener(folder) { message ->
            queue.put(message)
        }

        while (!messageChannel.isClosedForSend) {
            // Blocks until new event occurs.
            folder.idle()

            delay(QUEUE_LOAD_DELAY)

            while (queue.isNotEmpty()) {
                messageChannel.send(queue.remove())
            }
        }
    }

    fun shutdown() {
        store.close()
    }

    private fun noop(folder: IMAPFolder) = GlobalScope.launch {
        while (folder.isOpen) {
            delay(NOOP_DELAY)

            logger.debug("Sending NOOP to keep alive connection")

            folder.doCommand { protocol ->
                protocol.simpleCommand(NOOP_COMMAND, null)
            }
        }
    }

    private fun newMessageListener(
        folder: IMAPFolder,
        callback: (Message) -> Unit
    ) = folder.addMessageCountListener(object : MessageCountListener {
        override fun messagesAdded(event: MessageCountEvent?) {
            event?.messages?.forEach { message ->
                callback(convertMessage(message))
            }
        }

        override fun messagesRemoved(event: MessageCountEvent?) {
        }
    })

    private fun convertMessage(message: IMAPMessage): Message {
        return Message(
            subject = message.subject,
            from = message.from[0].toString(),
            recipients = message.allRecipients.map { recipient ->
                recipient.toString()
            },
            body = resolveContent(message),
            sentDate = DateTime(message.sentDate.time),
            flags = Flag.from(message.flags),
            backingMessage = message
        )
    }

    private fun resolveContent(message: IMAPMessage): String {
        val content = message.content

        return when(content) {
            is Multipart -> {
                (0 until content.count).fold("") { payload, index ->
                    val input = content.getBodyPart(index).inputStream
                    payload + input.bufferedReader().readText()
                }
            }
            is String -> content
            else ->
                throw RuntimeException("Cannot resolve content for mime type: ${message.contentType}")
        }
    }
}
