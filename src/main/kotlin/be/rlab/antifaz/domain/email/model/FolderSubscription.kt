package be.rlab.antifaz.domain.email.model

import kotlinx.coroutines.channels.Channel
import javax.mail.Folder

data class FolderSubscription(
    val folder: Folder,
    val channel: Channel<Message>
)
