package be.rlab.antifaz.domain.email.command

import be.rlab.antifaz.domain.MemorySlots.EMAILS_SLOT
import be.rlab.antifaz.domain.email.model.ChatConnectionInfo
import be.rlab.antifaz.domain.email.EmailService
import be.rlab.antifaz.domain.email.model.Flag
import be.rlab.antifaz.domain.email.model.Message
import be.rlab.antifaz.domain.inbox.InboxService
import be.rlab.antifaz.domain.inbox.model.Content
import be.rlab.tehanu.domain.AccessControl
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.Chat
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.TextMessage
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class AddEmailAccount(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    private val emailService: EmailService,
    private val inboxService: InboxService,
    private val accessControl: AccessControl,
    memory: Memory
) : Command {

    private val logger: Logger = LoggerFactory.getLogger(AddEmailAccount::class.java)
    private var connections: List<ChatConnectionInfo> by memory.slot(EMAILS_SLOT, listOf<ChatConnectionInfo>())

    override fun handle(
        context: MessageContext,
        message: TextMessage
    ): MessageContext {
        return try {
            context.parseInput(message.text, """
                Ahora mandame la información del servidor IMAPS y de autenticación. El formato es el siguiente:

                chat_id: 413c6d6a-65e3-4061-8072-5584630b68f8
                host: mail.rlab.be
                port: 993
                user_name: cthulhu
                password: rlyeh
            """.trimIndent()) { newConnectionInfo: ChatConnectionInfo ->
                resolveChat(
                    context = context,
                    connectionInfo = newConnectionInfo
                )?.let { targetChat ->
                    connections = resolveConnections(newConnectionInfo)

                    readInbox(context, targetChat)
                    context.talkTo(targetChat, "@${context.user.userName} registró una cuenta de correo para este chat.")
                }
            }
        } catch (cause: Exception) {
            context.talk("Hubo un error tratando de agregar la cuenta de email: ${cause.localizedMessage}")
            context.talk("Intentá de nuevo más tarde")
            context.done()
        }
    }

    private fun resolveChat(
        context: MessageContext,
        connectionInfo: ChatConnectionInfo
    ): Chat? {

        val targetChat: Chat? = accessControl.findChatById(connectionInfo.chatId)

        if (targetChat == null) {
            logger.info("[$context] Chat not found: ${connectionInfo.chatId}")
            context.talk("El chat con id ${connectionInfo.chatId} no existe.")
        }

        return targetChat
    }

    private fun resolveConnections(newConnectionInfo: ChatConnectionInfo): List<ChatConnectionInfo> {
        val exists = connections.any { existingConnectionInfo ->
            existingConnectionInfo.chatId == newConnectionInfo.chatId
        }

        return if (exists) {
            logger.info("Connection exists, removing: $newConnectionInfo")

            val index = connections.indexOfFirst { existingConnectionInfo ->
                existingConnectionInfo.chatId == newConnectionInfo.chatId
            }
            val newConnections = connections.toMutableList()
            newConnections.removeAt(index)
            newConnections + newConnectionInfo
        } else {
            logger.info("Adding new connection: $newConnectionInfo")
            connections + newConnectionInfo
        }
    }

    private fun readInbox(
        context: MessageContext,
        targetChat: Chat
    ) {
        emailService.findEmailReader(targetChat)?.let { emailReader ->
            val messages: List<Message> = emailReader.listMessages(
                emailReader.openFolder("INBOX"),
                Flag.UNSEEN
            )
            messages.forEach { message ->
                inboxService.broadcast(
                    chatId = targetChat.id,
                    from = message.from,
                    subject = message.subject,
                    content = Content.text(message.body),
                    timestamp = message.sentDate
                )
            }

            if (messages.isNotEmpty()) {
                context.talk("Tenés ${messages.size} mensaje(s) sin leer")
            } else {
                context.talk("Listo, no tenés mensajes sin leer")
            }
        }
    }
}
