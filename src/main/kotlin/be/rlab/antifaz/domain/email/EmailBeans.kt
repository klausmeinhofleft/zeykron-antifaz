package be.rlab.antifaz.domain.email

import be.rlab.antifaz.domain.email.command.AddEmailAccount
import be.rlab.tehanu.domain.model.ChatType
import org.springframework.context.support.beans

object EmailBeans {
    fun beans() = beans {
        // Services
        bean<EmailService>()

        bean {
            AddEmailAccount(
                name = "/email_add_account",
                scope = listOf(ChatType.PRIVATE),
                timeout = 60000,
                emailService = ref(),
                inboxService = ref(),
                accessControl = ref(),
                memory = ref()
            )
        }
    }
}
