package be.rlab.antifaz.domain

import be.rlab.tehanu.config.BotConfig
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.MessageListener
import be.rlab.tehanu.domain.model.EntityType
import be.rlab.tehanu.domain.model.TextMessage
import kotlin.random.Random

class CanHasKittenListener(
    private val botConfig: BotConfig
) : MessageListener<TextMessage> {

    companion object {
        private const val KITTIES_URL: String = "http://edgecats.net/first"
        private const val RANDOM_FROM: Int = 1000
        private const val RANDOM_TO: Int = 9999
    }

    override fun applies(
        context: MessageContext,
        message: TextMessage
    ): Boolean =
        message.hasEntity(EntityType.MENTION) ||
        message.hasEntity(EntityType.TEXT_MENTION)

    override fun handle(
        context: MessageContext,
        message: TextMessage
    ): MessageContext {
        (message.getEntity(EntityType.MENTION) ?:
        message.getEntity(EntityType.TEXT_MENTION))?.let { entity ->
            if (entity.value == botConfig.handle) {
                val randomId: Int = Random.nextInt(
                    RANDOM_FROM,
                    RANDOM_TO
                )
                context.talk("$KITTIES_URL?$randomId", disableWebPagePreview = false)
            }
        } ?: throw RuntimeException("Entity not found")

        return context.done()
    }
}