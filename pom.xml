<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>be.rlab</groupId>
    <artifactId>antifaz</artifactId>
    <packaging>jar</packaging>
    <version>1.10.3-SNAPSHOT</version>

    <!-- SCM -->
    <scm>
        <connection>scm:git:git@git.rlab.be:seykron/antifaz.git</connection>
        <developerConnection>scm:git:git@git.rlab.be:seykron/antifaz.git</developerConnection>
        <url>https://git.rlab.be/seykron/antifaz</url>
        <tag>antifaz-1.0.0</tag>
    </scm>

    <properties>
        <!-- Maven -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <junit.version>4.12</junit.version>
        <junit-jupiter.version>5.3.1</junit-jupiter.version>
        <reactor-test.version>3.2.5.RELEASE</reactor-test.version>
        <assertj.version>3.11.1</assertj.version>
        <mockito.version>2.28.2</mockito.version>
        <mockito-kotlin.version>2.1.0</mockito-kotlin.version>
        <bytebuddy.version>[1.9,)</bytebuddy.version>

        <qos.logback.logback-classic.version>1.0.11</qos.logback.logback-classic.version>
        <slf4j.all.version>1.7.2</slf4j.all.version>
        <jackson.mapper.version>2.9.8</jackson.mapper.version>
        <joda.version>2.10</joda.version>
        <telegram.version>0.3.6</telegram.version>
        <javax.email.version>1.5.2</javax.email.version>
        <dockerfile-maven-version>1.4.10</dockerfile-maven-version>
        <springframework.all.version>5.1.5.RELEASE</springframework.all.version>
        <zero-alloc-hash.version>0.9</zero-alloc-hash.version>
        <wiki-client.version>1.7.0</wiki-client.version>
        <h2.version>1.4.197</h2.version>
        <hikaricp.version>3.1.0</hikaricp.version>
        <typesafe.version>1.3.3</typesafe.version>

        <tehanu.version>2.0.2</tehanu.version>

        <kotlin.version>1.3.40</kotlin.version>
        <kotlin-coroutines.version>1.1.1</kotlin-coroutines.version>
        <kotlin.code.style>official</kotlin.code.style>
    </properties>

    <dependencies>
        <!-- Kotlin -->
        <dependency>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-stdlib-jdk8</artifactId>
            <version>${kotlin.version}</version>
        </dependency>

        <dependency>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-reflect</artifactId>
            <version>${kotlin.version}</version>
        </dependency>

        <dependency>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-test-junit</artifactId>
            <version>${kotlin.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.jetbrains.kotlinx</groupId>
            <artifactId>kotlinx-coroutines-core</artifactId>
            <version>${kotlin-coroutines.version}</version>
        </dependency>

        <!-- Test -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <version>${assertj.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>${junit-jupiter.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>${junit-jupiter.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <version>${mockito.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-inline</artifactId>
            <version>${mockito.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>net.bytebuddy</groupId>
            <artifactId>byte-buddy</artifactId>
            <version>${bytebuddy.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>net.bytebuddy</groupId>
            <artifactId>byte-buddy-agent</artifactId>
            <version>${bytebuddy.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.nhaarman.mockitokotlin2</groupId>
            <artifactId>mockito-kotlin</artifactId>
            <version>${mockito-kotlin.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>${springframework.all.version}</version>
            <scope>test</scope>
        </dependency>

        <!-- Dependency injection -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${springframework.all.version}</version>
        </dependency>
        <dependency>
            <groupId>javax.inject</groupId>
            <artifactId>javax.inject</artifactId>
            <version>1</version>
        </dependency>

        <!-- Logging -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.all.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>jcl-over-slf4j</artifactId>
            <version>${slf4j.all.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>jul-to-slf4j</artifactId>
            <version>${slf4j.all.version}</version>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>${qos.logback.logback-classic.version}</version>
        </dependency>

        <!-- Utils -->
        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
            <version>${joda.version}</version>
        </dependency>
        <dependency>
            <groupId>com.typesafe</groupId>
            <artifactId>config</artifactId>
            <version>${typesafe.version}</version>
        </dependency>

        <!-- Telegram -->
        <dependency>
            <groupId>be.rlab</groupId>
            <artifactId>tehanu</artifactId>
            <version>${tehanu.version}</version>
        </dependency>

        <!-- Email -->
        <dependency>
            <groupId>com.sun.mail</groupId>
            <artifactId>javax.mail</artifactId>
            <version>${javax.email.version}</version>
        </dependency>

        <!-- Content hashing -->
        <dependency>
            <groupId>net.openhft</groupId>
            <artifactId>zero-allocation-hashing</artifactId>
            <version>${zero-alloc-hash.version}</version>
        </dependency>

        <!-- Definitions command -->
        <dependency>
            <groupId>fastily</groupId>
            <artifactId>jwiki</artifactId>
            <version>${wiki-client.version}</version>
        </dependency>

        <!-- Data access -->
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <version>${h2.version}</version>
        </dependency>
        <dependency>
            <groupId>com.zaxxer</groupId>
            <artifactId>HikariCP</artifactId>
            <version>${hikaricp.version}</version>
        </dependency>
    </dependencies>

    <build>
        <finalName>antifaz</finalName>
        <sourceDirectory>${project.basedir}/src/main/kotlin</sourceDirectory>
        <testSourceDirectory>${project.basedir}/src/test/kotlin</testSourceDirectory>
        <plugins>
            <!-- Deploy plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
                <version>2.8.2</version>
            </plugin>
            <!-- Release plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>2.5.3</version>
                <!-- <inherited>true</inherited> -->
                <configuration>
                    <releaseProfiles>release</releaseProfiles>
                    <localCheckout>true</localCheckout>
                </configuration>
            </plugin>
            <!-- Jar plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>2.5</version>
                <configuration>
                    <excludes>
                        <!-- exclude any logback configuration from the jar -->
                        <exclude>**/logback.xml</exclude>
                        <!-- this line excludes env folder entirely from the jar -->
                        <exclude>**/conf/env/**</exclude>
                    </excludes>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.jetbrains.kotlin</groupId>
                <artifactId>kotlin-maven-plugin</artifactId>
                <version>${kotlin.version}</version>
                <executions>
                    <execution>
                        <id>compile</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>test-compile</id>
                        <phase>test-compile</phase>
                        <goals>
                            <goal>test-compile</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <jvmTarget>1.8</jvmTarget>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <configuration>
                    <archive>
                        <manifest>
                            <mainClass>be.rlab.antifaz.MainKt</mainClass>
                        </manifest>
                    </archive>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                </configuration>
                <executions>
                    <execution>
                        <id>make-assembly</id> <!-- this is used for inheritance merges -->
                        <phase>package</phase> <!-- bind to the packaging phase -->
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>com.spotify</groupId>
                <artifactId>dockerfile-maven-plugin</artifactId>
                <version>${dockerfile-maven-version}</version>
                <executions>
                    <execution>
                        <id>default</id>
                        <goals>
                            <goal>build</goal>
                            <goal>push</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <repository>registry.rlab.be/seykron/antifaz</repository>
                    <tag>${project.version}</tag>
                    <buildArgs>
                        <JAR_FILE>${project.build.finalName}-jar-with-dependencies.jar</JAR_FILE>
                    </buildArgs>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <!-- Repositories -->
    <repositories>
        <repository>
            <id>kotlin-eap</id>
            <name>Kotlin EAP Repository</name>
            <url>http://dl.bintray.com/kotlin/kotlin-eap</url>
        </repository>
        <repository>
            <id>jcenter</id>
            <name>JCenter Repository</name>
            <url>https://jcenter.bintray.com</url>
        </repository>
        <repository>
            <id>jitpack</id>
            <name>JitPack Repository</name>
            <url>https://jitpack.io</url>
        </repository>
    </repositories>

    <distributionManagement>
        <repository>
            <id>internal</id>
            <url>file://${MVN_LOCAL_REPO}</url>
        </repository>
    </distributionManagement>
</project>
